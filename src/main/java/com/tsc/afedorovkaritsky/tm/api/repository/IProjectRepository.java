package com.tsc.afedorovkaritsky.tm.api.repository;

import com.tsc.afedorovkaritsky.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findProjectById(final String id);

    Project findProjectByName(final String name);

    Project findProjectByIndex(final Integer index);

    Project removeProjectById(final String id);

    Project removeProjectByName(final String name);

    Project removeProjectByIndex(final Integer index);

    Project updateProjectById(final String id, final String name, final String description);

    Project updateProjectByIndex(final Integer index, final String name, final String description);

    int getCount();

}
