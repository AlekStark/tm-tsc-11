package com.tsc.afedorovkaritsky.tm.api.repository;

import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findTaskById(final String id);

    Task findTaskByName(final String name);

    Task findTaskByIndex(final Integer index);

    Task removeTaskById(final String id);

    Task removeTaskByName(final String name);

    Task removeTaskByIndex(final Integer index);

    Task updateTaskById(final String id, final String name, final String description);

    Task updateTaskByIndex(final Integer index, final String name, final String description);

    int getCount();

}
