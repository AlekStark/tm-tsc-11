package com.tsc.afedorovkaritsky.tm.api.service;

import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findTaskById(String id);

    Task findTaskByName(String name);

    Task findTaskByIndex(Integer index);

    Task removeTaskByIndex(Integer index);

    Task removeTaskByName(String name);

    Task removeTaskById(String id);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

}
