package com.tsc.afedorovkaritsky.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void showTaskById();

    void showTaskByName();

    void showTaskByIndex();

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByName();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
