package com.tsc.afedorovkaritsky.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void showProjectById();

    void showProjectByName();

    void showProjectByIndex();

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
