package com.tsc.afedorovkaritsky.tm.model;

import java.util.UUID;

public class Task {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return id + ": " + name + ";";
    }

}
