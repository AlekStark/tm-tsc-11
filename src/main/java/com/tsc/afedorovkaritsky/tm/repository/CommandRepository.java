package com.tsc.afedorovkaritsky.tm.repository;

import com.tsc.afedorovkaritsky.tm.api.repository.ICommandRepository;
import com.tsc.afedorovkaritsky.tm.constant.ArgumentConst;
import com.tsc.afedorovkaritsky.tm.constant.TerminalConst;
import com.tsc.afedorovkaritsky.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "О разработчике"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "О системе"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Помощь"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Версия"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Список аргументов"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Список команд"
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Создать задачу"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Удалить все задачи"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Показать список задач"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Создать проект"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Удалить все проекты"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Показать список проектов"
    );

    public static final Command PROJECT_BY_ID = new Command(
            TerminalConst.PROJECT_BY_ID, null,
            "Поиск проекта по Id"
    );

    public static final Command PROJECT_BY_NAME = new Command(
            TerminalConst.PROJECT_BY_NAME, null,
            "Поиск проекта по Имени"
    );

    public static final Command PROJECT_BY_INDEX = new Command(
            TerminalConst.PROJECT_BY_INDEX, null,
            "Поиск проекта по индексу"
    );

    public static final Command REMOVE_PROJECT_ID = new Command(
            TerminalConst.REMOVE_PROJECT_ID, null,
            "Удалить проект по Id"
    );

    public static final Command REMOVE_PROJECT_NAME = new Command(
            TerminalConst.REMOVE_PROJECT_NAME, null,
            "Удалить проект по имени"
    );

    public static final Command REMOVE_PROJECT_INDEX = new Command(
            TerminalConst.REMOVE_PROJECT_INDEX, null,
            "Удалить проект по индексу"
    );

    public static final Command UPDATE_PROJECT_INDEX = new Command(
            TerminalConst.UPDATE_PROJECT_INDEX, null,
            "Обновить проект по индексу"
    );

    public static final Command UPDATE_PROJECT_ID = new Command(
            TerminalConst.UPDATE_PROJECT_ID, null,
            "Обновить проект по ИД"
    );

    public static final Command TASK_BY_ID = new Command(
            TerminalConst.TASK_BY_ID, null,
            "Поиск задачи по Id"
    );

    public static final Command TASK_BY_NAME = new Command(
            TerminalConst.TASK_BY_NAME, null,
            "Поиск задачи по Имени"
    );

    public static final Command TASK_BY_INDEX = new Command(
            TerminalConst.TASK_BY_INDEX, null,
            "Поиск задачи по индексу"
    );

    public static final Command REMOVE_TASK_ID = new Command(
            TerminalConst.REMOVE_TASK_ID, null,
            "Удалить задачу по Id"
    );

    public static final Command REMOVE_TASK_NAME = new Command(
            TerminalConst.REMOVE_TASK_NAME, null,
            "Удалить задачу по имени"
    );

    public static final Command REMOVE_TASK_INDEX = new Command(
            TerminalConst.REMOVE_TASK_INDEX, null,
            "Удалить задачу по индексу"
    );

    public static final Command UPDATE_TASK_INDEX = new Command(
            TerminalConst.UPDATE_TASK_INDEX, null,
            "Обновить задачу по индексу"
    );

    public static final Command UPDATE_TASK_ID = new Command(
            TerminalConst.UPDATE_TASK_ID, null,
            "Обновить задачу по ИД"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Выход"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, INFO, VERSION, HELP, ARGUMENTS, COMMANDS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST, PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            PROJECT_BY_ID, PROJECT_BY_NAME, PROJECT_BY_INDEX, REMOVE_PROJECT_ID, REMOVE_PROJECT_NAME,
            REMOVE_PROJECT_INDEX, UPDATE_PROJECT_INDEX, UPDATE_PROJECT_ID,
            TASK_BY_ID, TASK_BY_NAME, TASK_BY_INDEX, REMOVE_TASK_ID, REMOVE_TASK_NAME,
            REMOVE_TASK_INDEX, UPDATE_TASK_INDEX, UPDATE_TASK_ID,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
