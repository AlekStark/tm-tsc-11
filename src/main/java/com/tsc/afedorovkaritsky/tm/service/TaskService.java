package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.api.service.ITaskService;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeTaskByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        return taskRepository.removeTaskByIndex(index);
    }

    @Override
    public Task removeTaskByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeTaskByName(name);
    }

    @Override
    public Task removeTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeTaskById(id);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.updateTaskById(id, name, description);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        return taskRepository.updateTaskByIndex(index, name, description);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findTaskById(id);
    }

    @Override
    public Task findTaskByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findTaskByName(name);
    }

    @Override
    public Task findTaskByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        return taskRepository.findTaskByIndex(index);
    }

}
