package com.tsc.afedorovkaritsky.tm.service;

import com.tsc.afedorovkaritsky.tm.api.repository.IProjectRepository;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectService;
import com.tsc.afedorovkaritsky.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        return projectRepository.removeProjectByIndex(index);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeProjectByName(name);
    }

    @Override
    public Project removeProjectById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeProjectById(id);
    }


    @Override
    public Project findProjectById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findProjectById(id);
    }

    @Override
    public Project findProjectByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findProjectByName(name);
    }

    @Override
    public Project findProjectByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        return projectRepository.findProjectByIndex(index);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.updateProjectById(id, name, description);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (index >= projectRepository.getCount()) return null;
        return projectRepository.updateProjectByIndex(index, name, description);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
