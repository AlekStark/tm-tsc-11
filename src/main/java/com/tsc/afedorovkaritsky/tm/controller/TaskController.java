package com.tsc.afedorovkaritsky.tm.controller;

import com.tsc.afedorovkaritsky.tm.api.controller.ITaskController;
import com.tsc.afedorovkaritsky.tm.api.service.ITaskService;
import com.tsc.afedorovkaritsky.tm.model.Task;
import com.tsc.afedorovkaritsky.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + "." + task);
            index++;
        }
        System.out.println("[OK]");
    }

    public void showTaskById() {
        System.out.println("[Введите ID задачи]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(id);
        if (task == null) {
            System.out.println("Задача с таким Id не существует");
            return;
        }
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        System.out.println("[Введите название задачи]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findTaskByName(name);
        if (task == null) {
            System.out.println("Задача с таким названием не существует");
            return;
        }
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findTaskByIndex(index);
        if (task == null) {
            System.out.println("Некорректный индекс");
            return;
        }
        showTask(task);
    }

    private void showTask(Task task) {
        if (task == null)
            return;
        System.out.println("ID: " + task.getId());
        System.out.println("Название: " + task.getName());
        System.out.println("Описание: " + task.getDescription());
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[Введите ID задачи]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskById(id);
        if (task == null) {
            System.out.println("Задача с таким Id не существует");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[Введите название задачи]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskByName(name);
        if (task == null) {
            System.out.println("Задача с таким названием не существует");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeTaskByIndex(index);
        if (task == null) {
            System.out.println("Некорректный индекс");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[Введите ид]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(id);
        if (task == null) {
            System.out.println("Некорректный Id");
            return;
        }
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskById(id, name, description);
        if (taskUpdate == null) System.out.println("Некорректный индекс");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findTaskByIndex(index);
        if (task == null) {
            System.out.println("Некорректный индекс");
            return;
        }
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdate == null) System.out.println("Некорректный индекс");
    }

}
