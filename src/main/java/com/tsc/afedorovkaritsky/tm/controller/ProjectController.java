package com.tsc.afedorovkaritsky.tm.controller;

import com.tsc.afedorovkaritsky.tm.api.controller.IProjectController;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectService;
import com.tsc.afedorovkaritsky.tm.model.Project;
import com.tsc.afedorovkaritsky.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + "." + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание проекта]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[Введите ID проекта]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectById(id);
        if (project == null) {
            System.out.println("Проект с таким Id не существует");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectByName(name);
        if (project == null) {
            System.out.println("Проект с таким названием не существует");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeProjectByIndex(index);
        if (project == null) {
            System.out.println("Некорректный индекс");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[Введите ид]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null) {
            System.out.println("Некорректный Id");
            return;
        }
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(id, name, description);
        if (projectUpdate == null) System.out.println("Некорректный индекс");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findProjectByIndex(index);
        if (project == null) {
            System.out.println("Некорректный индекс");
            return;
        }
        System.out.println("[Введите название]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Введите описание]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdate == null) System.out.println("Некорректный индекс");
    }

    public void showProjectById() {
        System.out.println("[Введите ID проекта]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(id);
        if (project == null) {
            System.out.println("Проект с таким Id не существует");
            return;
        }
        showProject(project);
    }

    @Override
    public void showProjectByName() {
        System.out.println("[Введите название проекта]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findProjectByName(name);
        if (project == null) {
            System.out.println("Проект с таким названием не существует");
            return;
        }
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[Введите индекс]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findProjectByIndex(index);
        if (project == null) {
            System.out.println("Некорректный индекс");
            return;
        }
        showProject(project);
    }

    private void showProject(Project project) {
        if (project == null)
            return;
        System.out.println("ID: " + project.getId());
        System.out.println("Название: " + project.getName());
        System.out.println("Описание: " + project.getDescription());
    }

}
