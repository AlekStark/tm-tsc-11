package com.tsc.afedorovkaritsky.tm.constant;

public class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_LIST = "task-list";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_BY_ID = "project-id";

    public static final String PROJECT_BY_NAME = "project-name";

    public static final String PROJECT_BY_INDEX = "project-index";

    public static final String PROJECT_LIST = "project-list";

    public static final String REMOVE_PROJECT_ID = "remove-project-id";

    public static final String REMOVE_PROJECT_NAME = "remove-project-name";

    public static final String REMOVE_PROJECT_INDEX = "remove-project-index";

    public static final String UPDATE_PROJECT_ID = "update-project-id";

    public static final String UPDATE_PROJECT_INDEX = "update-project-index";

    public static final String TASK_BY_ID = "task-id";

    public static final String TASK_BY_NAME = "task-name";

    public static final String TASK_BY_INDEX = "task-index";

    public static final String REMOVE_TASK_ID = "remove-task-id";

    public static final String REMOVE_TASK_NAME = "remove-task-name";

    public static final String REMOVE_TASK_INDEX = "remove-task-index";

    public static final String UPDATE_TASK_ID = "update-task-id";

    public static final String UPDATE_TASK_INDEX = "update-task-index";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

}
